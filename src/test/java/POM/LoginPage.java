package POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;



public class LoginPage {


	WebDriver Driver;
	
	By Continue = By.xpath("//button[@type ='submit']");
	By Password = By.id("password");
	By UN = By.id("email");
	By Submit = By.id("login-button");
	
	By EmailRequired_ErrorMessage = By.id("email-required");
	By PasswordRequired_ErrorMessage = By.id("password-required");
	By ErrorMessage_InTerminal  = By.id("error-message");
	
	
	public LoginPage(WebDriver Driver) {
        this.Driver = Driver;
    }
	
	
	public void Wait(WebElement Element) {
		WebDriverWait WW = new WebDriverWait(Driver , 25);
		WW.until(ExpectedConditions.visibilityOf(Element));
	}
	
	public void UserLogin() throws InterruptedException {

	WebElement User = Driver.findElement(Continue);
	Wait(User);
	User.click();
	
	}
	
	public void LoginByEmail(String EmailID) throws InterruptedException {

		WebElement Web = Driver.findElement(UN);
		Wait(Web);
		Web.sendKeys(EmailID);

		}
	
	public void LoginByPassword(String pass) throws InterruptedException {

		WebElement Web = Driver.findElement(Password);
		Wait(Web);
		Web.sendKeys(pass);

		
		}
	
	public void Submit() {

		WebElement Web = Driver.findElement(Submit);
		Wait(Web);
		Web.click();

		
		}
	
	public void ValidateError_EmailRequired(String ActualError) {

		WebElement EmailRequiredErrorInTerminalTextBox = Driver.findElement(EmailRequired_ErrorMessage);
		Wait(EmailRequiredErrorInTerminalTextBox);
	    String ExpectedErrorMessage = EmailRequiredErrorInTerminalTextBox.getText();
	    System.out.println(ExpectedErrorMessage);
	    Assert.assertEquals(ExpectedErrorMessage , ActualError);
		}
	
	public void ValidateError_PasswordRequired(String ActualError) {

		WebElement PasswordRequiredErrorInTerminalTextBox = Driver.findElement(PasswordRequired_ErrorMessage);
		Wait(PasswordRequiredErrorInTerminalTextBox);
	    String ExpectedErrorMessage = PasswordRequiredErrorInTerminalTextBox.getText();
	    System.out.println(ExpectedErrorMessage);
	    Assert.assertEquals(ExpectedErrorMessage , ActualError);
		}
	
	public void ValidateError_InTerminalTextField(String ActualError) {

		WebElement ErrorInTerminalTextBox = Driver.findElement(ErrorMessage_InTerminal);
		Wait(ErrorInTerminalTextBox);
	    String ExpectedErrorMessage = ErrorInTerminalTextBox.getText();
	    System.out.println(ExpectedErrorMessage);
	    Assert.assertEquals(ExpectedErrorMessage , ActualError);
		}
	
}
