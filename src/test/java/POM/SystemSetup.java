package POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SystemSetup {

WebDriver Driver;
By UserMangementButton = By.xpath("//a[contains(text(),'User management')]");
		
public SystemSetup(WebDriver Driver) {
	        this.Driver = Driver;
	    }
		
public void Wait(WebElement Element) {
	WebDriverWait WW = new WebDriverWait(Driver , 25);
	WW.until(ExpectedConditions.visibilityOf(Element));
}
		
public void Click_UserManagement() {
WebElement UserMangement = Driver.findElement(UserMangementButton);
		Wait(UserMangement);
		UserMangement.click();
		}
	}

