package POM;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.ActionFactory;

public class UserManagement {
	
	WebDriver Driver;
	ActionFactory AF;

	
	By FirstName = By.xpath("//input[@data-placeholder = 'First name']");
	By LastName = By.xpath("//input[@data-placeholder = 'Last name']");
	By PhoneNumber = By.xpath("//input[@data-placeholder = 'Phone number']");
	By Save = By.xpath("//mat-icon[@svgicon='save']");
	By CANCEL = By.xpath("//mat-icon[@data-mat-icon-name='cancel']");
	By EmailID = By.xpath("//mat-row//mat-cell//input[@type='email']");
	By SidePanel = By.xpath("//mat-icon[@data-mat-icon-name='circle_spread']");
	By CancelSidePanel = By.xpath("//button[@class = 'mat-focus-indicator close-button mat-raised-button mat-button-base ng-star-inserted']");
	By SavePanel = By.xpath("//button[@class = 'mat-focus-indicator submit-button mat-raised-button mat-button-base ng-star-inserted']");
	By ActionDropdown = By.xpath("//combitime-action-panel");
	By SendInvite = By.xpath("//combitime-action-panel//div[contains(text(),'Send invite')]");
	By Add_Button = By.xpath("//mat-icon[@svgicon='add_light_bg']");
	
	
	public UserManagement(WebDriver Driver) {
		this.Driver = Driver;
	}
//================================================================================================================
	public void Wait(WebElement Element) {
		WebDriverWait WW = new WebDriverWait(Driver , 25);
		WW.until(ExpectedConditions.visibilityOf(Element));
	}
	
	public void UserManagementTitle(String Title) {
		AF.ValidatePageTitle(Title);
	}
	

	public String Recent_UserLoginID(String LoginID_Gmail) throws InterruptedException {
		WebDriverWait WW = new WebDriverWait(Driver , 40);
		WebElement LoginID = Driver.findElement(By.xpath("//mat-row//mat-cell//span[contains(text(),'"+LoginID_Gmail+"')]"));
	    WW.until(ExpectedConditions.visibilityOf(LoginID));
		return LoginID.getText();
	}

	public void ChooseCheckboxToSendInvite(String LoginID_Gmail) {

		 WebDriverWait WW = new WebDriverWait(Driver , 40);
			Actions A = new Actions(Driver);
			WebElement CheckBox = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]/../../..//mat-cell//mat-checkbox"));
			WW.until(ExpectedConditions.visibilityOf(CheckBox));
			A.moveToElement(CheckBox).click().perform();
	}
	
//======================================================================================================FUNCTIONS=====================
//====================FUNCTIONS================================
	
	
public void FillFirstName(String FN) {
		WebElement IstName = Driver.findElement(FirstName);
		Wait(IstName);
		IstName.sendKeys(FN);
}
public void FillLastName(String LN) {
	WebElement Last_Name = Driver.findElement(LastName);
	Wait(Last_Name);
	Last_Name.sendKeys(LN);
}
public void FillEmailID(String email) {
	WebElement Email_ID = Driver.findElement(EmailID);
	Wait(Email_ID);
	Email_ID.sendKeys(email);
}

public void FillPhoneNumber(String Number) {
	WebElement PhoneNum = Driver.findElement(PhoneNumber);
	Wait(PhoneNum);
	PhoneNum.sendKeys(Number);
}

public void ClickOnSave(){
	WebElement SaveButton = Driver.findElement(Save);
	Wait(SaveButton);
	SaveButton.click();
}

public void ClickOnSidepanelIcon() {
	WebElement SidePanelIcon = Driver.findElement(SidePanel);
	Wait(SidePanelIcon);
	SidePanelIcon.click();
}

public void ClickOnSidepanel_Save() {
	WebElement SaveButton_SidePanel = Driver.findElement(SavePanel);
	Wait(SaveButton_SidePanel);
	SaveButton_SidePanel.click();
}

public void ClickOnSidepanel_Cancel() {
	WebElement CancelButton_SidePanel = Driver.findElement(CancelSidePanel);
	Wait(CancelButton_SidePanel);
	CancelButton_SidePanel.click();
}

public void Cancel() {
	WebElement CancelRow = Driver.findElement(CANCEL);
	Wait(CancelRow);
	CancelRow.click();
}



public void SetLanguageFromDropdown(String LanguageName) throws InterruptedException {
	List<WebElement> W = Driver.findElements(By.xpath("//mat-row"));
	int TotalRow = W.size();
	WebElement LanguageCell = Driver.findElement(By.xpath("//mat-row["+TotalRow+"]//mat-cell[7]"));
	Wait(LanguageCell);
	LanguageCell.click();
	Thread.sleep(2000);
	WebElement LanguageOption =  Driver.findElement(By.xpath("//mat-list-option[@title = '"+LanguageName+"']"));
	Wait(LanguageOption);
	LanguageOption.click();
	
}

public void SetPrimaryFromDropdown(String Primary) throws InterruptedException {
	List<WebElement> W = Driver.findElements(By.xpath("//mat-row"));
	int TotalRow = W.size();
	WebElement PrimaryCell = Driver.findElement(By.xpath("//mat-row["+TotalRow+"]//mat-cell[8]"));
	Wait(PrimaryCell);
	PrimaryCell.click();
	Thread.sleep(2000);
	WebElement PrimaryOption =  Driver.findElement(By.xpath("//mat-list-option[@title = '"+Primary+"']"));
	Wait(PrimaryOption);
	PrimaryOption.click();
	
}

public void SidePanelFlag_Role(String text) {
	WebDriverWait WW = new WebDriverWait(Driver , 40);
	WebElement W = Driver.findElement(By.xpath("//span[contains(text(),'"+text+"')]"));
	WW.until(ExpectedConditions.visibilityOf(W));
	W.click();
}

public void ValidateCreatedRow(String LoginID_Gmail , String Gmail) throws InterruptedException {
	WebDriverWait WW = new WebDriverWait(Driver , 40);
	WebElement Email = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]"));
	WW.until(ExpectedConditions.visibilityOf(Email));
	Assert.assertEquals(Email.getText(), Gmail);

}

public void EditCreatedRow(String LoginID_Gmail) throws InterruptedException {
	WebDriverWait WW = new WebDriverWait(Driver , 40);
	Actions A = new Actions(Driver);
	WebElement LoginID = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]"));
	A.moveToElement(LoginID).perform();
	WebElement EditButton = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]/../../..//mat-cell//button//mat-icon[@data-mat-icon-name='edit']"));
	WW.until(ExpectedConditions.visibilityOf(EditButton));
	A.moveToElement(EditButton).click().perform();
}

public void DeleteCreatedRow(String LoginID_Gmail) throws InterruptedException {
	WebDriverWait WW = new WebDriverWait(Driver , 40);
	Actions A = new Actions(Driver);
	WebElement LoginID = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]"));
	A.moveToElement(LoginID).perform();
	WebElement DeleteButton = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]/../../..//mat-cell//button//mat-icon[@data-mat-icon-name='delete']"));
	WW.until(ExpectedConditions.visibilityOf(DeleteButton));
	A.moveToElement(DeleteButton).click().perform();
	//Thread.sleep(10000);
	//DeleteButton.click();
	WebElement deleteAlertOK = Driver.findElement(By.xpath("//button[@class='mat-focus-indicator mat-raised-button mat-button-base delete-button ng-star-inserted']"));
	WW.until(ExpectedConditions.visibilityOf(deleteAlertOK));
	A.moveToElement(deleteAlertOK).click().perform();
	
}     

public void MakeUserActive(String LoginID_Gmail) {
	
	 WebDriverWait WW = new WebDriverWait(Driver , 40);
		Actions A = new Actions(Driver);
		WebElement LoginID = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]"));
		A.moveToElement(LoginID).perform();
		WebElement ToggleActive = Driver.findElement(By.xpath("//mat-row//mat-cell//p[contains(text(),'"+LoginID_Gmail+"')]/../../..//mat-cell//span//mat-slide-toggle"));
		WW.until(ExpectedConditions.visibilityOf(ToggleActive));
		A.moveToElement(ToggleActive).click().perform();
	
}	


//public void Edit(String FN , String LN , String email , String LoginID) throws InterruptedException {
//	
//	DF.SendKey(Email, DF.Recent_UserLoginID(LoginID));	
//  
//}




//
//public void SendInviteToNewUsers(String email) throws InterruptedException {
//	
//	Thread.sleep(2000);
//	DF.Click(ActionDropdown);
//    DF.Click(SendInvite);
//    DF.Validate_ToastMessage("Please select a row to proceed");
//    DF.ChooseCheckboxToSendInvite(email);
//    DF.Click(ActionDropdown);
//    DF.Click(SendInvite);
//    DF.Validate_ToastMessage("Please check your email for further process.");
//}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	
	

}
