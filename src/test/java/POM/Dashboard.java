package POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Dashboard {
	WebDriver Driver;
	
By SystemSetup = By.xpath("//span[contains(text(),'System setup  ')]");
	
	
	public Dashboard(WebDriver Driver) {
        this.Driver = Driver;
        
	}
	
	
	public void Wait(WebElement Element) {
		WebDriverWait WW = new WebDriverWait(Driver , 25);
		WW.until(ExpectedConditions.visibilityOf(Element));
	}
	
	public void Click_SystemSetup() {

	WebElement SystemSetupButton = Driver.findElement(SystemSetup);
	Wait(SystemSetupButton);
	SystemSetupButton.click();
	
	}
}
