package StepsDefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import MyHooks.StepsHooks;
import POM.Dashboard;
import POM.LoginPage;
import POM.SystemSetup;
import POM.UserManagement;
import Utility.ActionFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Steps {
	
 WebDriver Driver;
	LoginPage LP;
	ActionFactory AF;
	Dashboard D;
	UserManagement UM;
	SystemSetup SS;
	

	
	@Given("Chrome Launch")
	public void chrome_Launch() {
		 
		    System.setProperty("webdriver.chrome.driver" , "./src/test/resources/Driver/chromedriver.exe");
			Driver = new ChromeDriver();
	
 
	}

	@When("Navigate to URL {string}")
	public void navigate_to_URL(String string) {
	  
		Driver.navigate().to("https://combitimedevapp.z16.web.core.windows.net/login");
		Driver.manage().window().maximize();
		Driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
	}

	@When("Click on USER to login Admin")
	public void click_on_USER_to_login_Admin() throws InterruptedException {
		LP = new LoginPage(Driver);
		LP.UserLogin();    
	}

	@When("Login By Email as {string}")
	public void login_By_Email_as(String string) throws InterruptedException {
		LP = new LoginPage(Driver);
	   LP.LoginByEmail(string);
	}
	
	@When("Login By Password as {string}")
	public void login_By_Password_as(String string) throws InterruptedException {
		LP = new LoginPage(Driver);
	   LP.LoginByPassword(string);
	}

	@When("Click on Submit button")
	public void click_on_Submit_button() throws InterruptedException {
		LP = new LoginPage(Driver);
	   LP.Submit();
	}
	
	@Then("Validate Page Title {string}")
	public void validate_Page_Title(String title) {
		AF = new ActionFactory(Driver);
	 AF.ValidatePageTitle(title);
	}

	@Given("Admin logged in")
	public void admin_logged_in() {
	    
		AF = new ActionFactory(Driver);
		 AF.ValidatePageTitle("Dashboard");
	}

	@When("Go in System Setup")
	public void go_in_System_Setup() {
	    D = new Dashboard(Driver);
	    D.Click_SystemSetup();
	}

	@When("Click on UserManagement")
	public void click_on_UserManagement() {
	    SS = new SystemSetup(Driver);
	    SS.Click_UserManagement();
	}

	@When("Click on Add")
	public void click_on_Add() throws InterruptedException {
		Thread.sleep(5000);
		AF = new ActionFactory(Driver);
		AF.ClickOnAdd();
	}

	@When("Fill First Name {string}")
	public void fill_First_Name(String string) {
	   UM = new UserManagement(Driver);
			   UM.FillFirstName(string);
	}

	@When("Fill Last Name {string}")
	public void fill_Last_Name(String string) {
		UM = new UserManagement(Driver);
		UM.FillLastName(string);
	}

	@When("Fill EmailID {string}")
	public void fill_EmailID(String string) {
		UM = new UserManagement(Driver);
		UM.FillEmailID(string);
	}

	@When("Fill PhoneNumber {string}")
	public void fill_PhoneNumber(String string) {
		UM = new UserManagement(Driver);
		UM.FillPhoneNumber(string);
	}

	@When("Flag on in Side Panel of {string}")
	public void flag_on_in_Side_Panel_of(String string) {
		UM = new UserManagement(Driver);
		UM.SidePanelFlag_Role(string);
	}

	@Then("Validate Toast Message {string}")
	public void validate_Toast_Message(String string) {
		AF = new ActionFactory(Driver);
		 AF.Validate_ToastMessage(string);
	}
	
	@When("Set language from dropdown {string}")
	public void set_language_from_dropdown(String string) throws InterruptedException {
		UM = new UserManagement(Driver);
		UM.SetLanguageFromDropdown(string);
	}

	@When("Set Primary from Dropdown {string}")
	public void set_Primary_from_Dropdown(String string) throws InterruptedException {
		UM = new UserManagement(Driver);
		UM.SetPrimaryFromDropdown(string);
	}

	@When("Click on Save button")
	public void click_on_Save_button() {
		UM = new UserManagement(Driver);
		UM.ClickOnSave(); 
	}
	
	@When("Close Side Panel")
	public void close_Side_Panel() {
		UM = new UserManagement(Driver);
		UM.ClickOnSidepanelIcon();
	}
	

@Then("Validate error In Terminal Text Field {string}")
public void validate_error_In_Terminal_Text_Field(String string) {
	 LP = new LoginPage(Driver);
	    LP.ValidateError_InTerminalTextField(string);
}

@Then("Validate error Email Required {string}")
public void validate_error_Email_Required(String string) {
	 LP = new LoginPage(Driver);
	    LP.ValidateError_EmailRequired(string);
}

@Then("Validate error Password Required {string}")
public void validate_error_Password_Required(String string) {
	 LP = new LoginPage(Driver);
	    LP.ValidateError_PasswordRequired(string);
}


@Then("Quit Window")
public void quit_Window() throws InterruptedException {
	Thread.sleep(5000);
    Driver.quit();

}




}
