package MyHooks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

import StepsDefinition.Steps;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class StepsHooks {
	
	WebDriver Driver;
	
	
	
	@Before
	public void Setup() {
		
		System.out.println("Launched Chrome");
	}
	
	
	@AfterMethod
	public void TearDownDriver() {
		
		System.out.println("Okay");
		Driver.quit();
		
		
	}

}
