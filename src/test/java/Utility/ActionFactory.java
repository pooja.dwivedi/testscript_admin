package Utility;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionFactory {
	
	WebDriver Driver;
	
	public ActionFactory(WebDriver Driver) {
		this.Driver = Driver;
	}
	

public void ValidatePageTitle(String Text) {
	WebDriverWait WW = new WebDriverWait(Driver , 40);
		WebElement Title = Driver.findElement(By.xpath("//h3"));
		WW.until(ExpectedConditions.visibilityOf(Title));
		// SoftAssert SA = new SoftAssert();
		 Assert.assertEquals(Title.getText(), Text);
}
public void Wait(WebElement Element) {
	WebDriverWait WW = new WebDriverWait(Driver , 25);
	WW.until(ExpectedConditions.visibilityOf(Element));
}
	
public void ScrollDown() {
	
	JavascriptExecutor js = (JavascriptExecutor) Driver;
	js.executeScript("window.scrollBy(0,600)", "");
	
}

public void ScrollToElement(WebElement W) {
	JavascriptExecutor js = (JavascriptExecutor) Driver;
	 js.executeScript("arguments[0].scrollIntoView(true)", W);
}

public void Scroll_SubScreen() throws InterruptedException {
	
	EventFiringWebDriver EFD = new EventFiringWebDriver(Driver);
	EFD.executeScript("document.querySelector('mat-dialog-content[class = \"mat-dialog-content\"]').scrollTop = 500");
	Thread.sleep(3000);
}

public void Scroll_horizontalTable() throws InterruptedException {
	
	JavascriptExecutor js = (JavascriptExecutor) Driver;
	js.executeScript("document.querySelector('div[class = \"combitime-table-container\"]').scrollIntoView();");
	Thread.sleep(3000);
}

public void Validate_ToastMessage(String Message) {
	
	WebDriverWait WW = new WebDriverWait(Driver , 40);
	WebElement Alert = Driver.findElement(By.xpath("//combitime-notification"));
	WW.until(ExpectedConditions.visibilityOf(Alert));
	System.out.println (Alert.getText());
    Assert.assertEquals(Alert.getText(), Message);

}


//this created for validate email id generated when created new User.




//__________________________________________________________________________________________
//__________________________________________________________________________________________

public void AddApprovalGroup(String Name , String Discription) throws InterruptedException {
	 WebDriverWait WW = new WebDriverWait(Driver , 40);
		Actions A = new Actions(Driver);
		WebElement ApprovalGroupName = Driver.findElement(By.xpath("//combitime-table-input-column//mat-form-field//input[@data-placeholder='Enter name']"));
		WW.until(ExpectedConditions.visibilityOf(ApprovalGroupName));
		ApprovalGroupName.sendKeys(Name);
		
		WebElement Description = Driver.findElement(By.xpath("//mat-cell[2]//input[@data-placeholder='Enter description']"));
		WW.until(ExpectedConditions.visibilityOf(Description));
		Description.sendKeys(Discription);
		
		WebElement TypeDrpDwn = Driver.findElement(By.xpath("//combitime-select-list//span[contains(text(),' Select type ')]"));
		WW.until(ExpectedConditions.visibilityOf(TypeDrpDwn));
		TypeDrpDwn.click();
		
		WebElement GroupType = Driver.findElement(By.xpath("//mat-list-option//span[contains(text(),' Quality control admin ')]"));
		WW.until(ExpectedConditions.visibilityOf(GroupType));
		WW.until(ExpectedConditions.elementToBeClickable(GroupType));
		A.moveToElement(GroupType).click().perform();
}

public void EditApprovalGroup(String ApprovalGroupName) throws InterruptedException {
	WebDriverWait WW = new WebDriverWait(Driver , 40);
	Actions A = new Actions(Driver);
	WebElement ApprovalName = Driver.findElement(By.xpath("//mat-row//mat-cell//span[contains(text(),'"+ApprovalGroupName+"')]"));
	A.moveToElement(ApprovalName).perform();
	WebElement EditButton = Driver.findElement(By.xpath("//mat-row//mat-cell//span[contains(text(),'"+ApprovalGroupName+"')]/../../..//mat-cell//button//mat-icon[@data-mat-icon-name='edit']"));
	WW.until(ExpectedConditions.visibilityOf(EditButton));
	A.moveToElement(EditButton).click().perform();
	
	}

public void DeleteApprovalGroup(String ApprovalGroupName) throws InterruptedException {
	WebDriverWait WW = new WebDriverWait(Driver , 40);
	Actions A = new Actions(Driver);
	WebElement ApprovalName = Driver.findElement(By.xpath("//mat-row//mat-cell//span[contains(text(),'"+ApprovalGroupName+"')]"));
	A.moveToElement(ApprovalName).perform();
	WebElement DeleteButton = Driver.findElement(By.xpath("//mat-row//mat-cell//span[contains(text(),'"+ApprovalGroupName+"')]/../../..//mat-cell//button//mat-icon[@data-mat-icon-name='delete']"));
	WW.until(ExpectedConditions.visibilityOf(DeleteButton));
	A.moveToElement(DeleteButton).click().perform();
	
	WebElement deleteAlertOK = Driver.findElement(By.xpath("//button[@class='mat-focus-indicator mat-raised-button mat-button-base delete-button ng-star-inserted']"));
	WW.until(ExpectedConditions.visibilityOf(deleteAlertOK));
	A.moveToElement(deleteAlertOK).click().perform();
	
	
	}
//public void ClickONAdd_Button() {
//	WebDriverWait WW = new WebDriverWait(Driver , 25);
//	WebElement Element = Driver.findElement(By.xpath("//mat-icon[@svgicon='add_light_bg']"));
//	WW.until(ExpectedConditions.visibilityOf(Element));
//	Element.click();
//	
//}

public void ClickOnAdd() {
	WebElement Add = Driver.findElement(By.xpath("//mat-icon[@svgicon='add_light_bg']"));
	Wait(Add);
	Add.click();	
}

public void ClickOnSave() {
	
}



}

