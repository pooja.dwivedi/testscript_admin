Feature: Login Feature

  @Login
  Scenario Outline: Login using valid credentials
    Given Chrome Launch
    When Navigate to URL "https://combitimedevapp.z16.web.core.windows.net/login"
    And Click on USER to login Admin
    And Login By Email as "<EmailID>"
    And Login By Password as "<Password>"
    And Click on Submit button
    Then Validate Page Title "Dashboard"
    And Quit Window
    

    Examples: 
      | EmailID                       |  | Password   |
      | pooja.dwivedi+21@codinova.com |  | System123# | 

  @Login
  Scenario Outline: Login using Invalid Pasword
    Given Chrome Launch
    When Navigate to URL "https://combitimedevapp.z16.web.core.windows.net/login"
    And Click on USER to login Admin
    And Login By Email as "<EmailID>"
    And Login By Password as "<Password>"
    And Click on Submit button
    Then Validate error In Terminal Text Field "Invalid username or password"
    And Quit Window

    Examples: 
      | EmailID                       |  | Password     |
      | pooja.dwivedi+21@codinova.com |  | Syst76em123# |

  @Login
  Scenario Outline: Login using Invalid UserID
    Given Chrome Launch
    When Navigate to URL "https://combitimedevapp.z16.web.core.windows.net/login"
    And Click on USER to login Admin
    And Login By Email as "<EmailID>"
    And Login By Password as "<Password>"
    And Click on Submit button
    Then Validate error In Terminal Text Field "Invalid username or password"
    And Quit Window

    Examples: 
      | EmailID                         |  | Password   |
      | pooja.dwived87i+21@codinova.com |  | System123# |

  @Login
  Scenario Outline: Login using Empty Email & Password
    Given Chrome Launch
    When Navigate to URL "https://combitimedevapp.z16.web.core.windows.net/login"
    And Click on USER to login Admin
    And Login By Email as "<EmailID>"
    And Login By Password as "<Password>"
    And Click on Submit button
    Then Validate error Email Required "Email is Required"
    Then Validate error Password Required "Password is Required"
    And Quit Window

    Examples: 
      | EmailID |  | Password |
      |         |  |          |

  @Login
  Scenario Outline: Login using Empty  Password
    Given Chrome Launch
    When Navigate to URL "https://combitimedevapp.z16.web.core.windows.net/login"
    And Click on USER to login Admin
    And Login By Email as "<EmailID>"
    And Login By Password as "<Password>"
    And Click on Submit button
    Then Validate error Password Required "Password is Required"
    And Quit Window

    Examples: 
      | EmailID                       |  | Password |
      | pooja.dwivedi+21@codinova.com |  |          |
