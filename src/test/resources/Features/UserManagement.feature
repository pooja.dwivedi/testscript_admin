Feature: Crud in User management Screen

  Background: Login Before UserManagement
    Given Chrome Launch
    When Navigate to URL "https://combitimedevapp.z16.web.core.windows.net/login"
    And Click on USER to login Admin
    And Login By Email as "pooja.dwivedi+21@codinova.com"
    And Login By Password as "System123#"
    And Click on Submit button
    Then Validate Page Title "Dashboard"

  
  Scenario Outline: ADD NEW USER
    Given Admin logged in
    When Go in System Setup
    And Click on UserManagement
    Then Validate Page Title "User management"
    When Click on Add
    And Fill First Name "<First_Name>"
    And Fill Last Name "<Last_Name>"
    And Fill EmailID "<Email>"
    And Flag on in Side Panel of "Work time planning"
    And Close Side Panel
    And Fill PhoneNumber "<MobileNumber>"
    And Set language from dropdown "English"
    And Set Primary from Dropdown "Dashboard"
    And Click on Save button
    Then Validate Toast Message "User created successfully"

    Examples: 
      | First_Name |  | Last_Name  |  | Email               |  | MobileNumber |
      | Pcucumber4 |  | Lcucumber4 |  | Cucu1mber4@gmail.com |  |    767621762 |

 
  Scenario Outline: ADD DUPLICATE USER
    Given Admin logged in
    When Go in System Setup
    And Click on UserManagement
    Then Validate Page Title "User management"
    When Click on Add
    And Fill First Name "<First_Name>"
    And Fill Last Name "<Last_Name>"
    And Fill EmailID "<Email>"
    And Flag on in Side Panel of "Work time planning"
    And Close Side Panel
    And Fill PhoneNumber "<MobileNumber>"
    And Set language from dropdown "English"
    And Set Primary from Dropdown "Dashboard"
    And Click on Save button
    Then Validate Toast Message "Account already exists, try to login with your current credentials."

    Examples: 
      | First_Name |  | Last_Name  |  | Email               |  | MobileNumber |
      | Pcucumber4 |  | Lcucumber4 |  | Cucu1mber4@gmail.com |  |    767621762 |


  Scenario Outline: NO ROLE ACCESS TO USER
    Given Admin logged in
    When Go in System Setup
    And Click on UserManagement
    Then Validate Page Title "User management"
    When Click on Add
    And Fill First Name "<First_Name>"
    And Fill Last Name "<Last_Name>"
    And Fill EmailID "<Email>"
    And Close Side Panel
    And Fill PhoneNumber "<MobileNumber>"
    And Set language from dropdown "English"
    And Set Primary from Dropdown "Dashboard"
    And Click on Save button
    Then Validate Toast Message "Select user role"

    Examples: 
      | First_Name |  | Last_Name  |  | Email               |  | MobileNumber |
      | Pcucumber1 |  | Lcucumber1 |  | Cucu1mber@gmail.com |  |    767621762 |

 
  Scenario Outline: INVALID EMAIL ID GIVEN TO USER ROW
    Given Admin logged in
    When Go in System Setup
    And Click on UserManagement
    Then Validate Page Title "User management"
    When Click on Add
    And Fill First Name "<First_Name>"
    And Fill Last Name "<Last_Name>"
    And Fill EmailID "<Email>"
    And Flag on in Side Panel of "Work time planning"
    And Close Side Panel
    And Fill PhoneNumber "<MobileNumber>"
    And Set language from dropdown "English"
    And Set Primary from Dropdown "Dashboard"
    And Click on Save button
    Then Validate Toast Message "Email address is invalid"

    Examples: 
      | First_Name |  | Last_Name  |  | Email      |  | MobileNumber |
      | Pcucumber1 |  | Lcucumber1 |  | Cucu1mber@ |  |    767621762 |

 
  Scenario Outline: ADD NEW USER USING MANDATORY FIELD
    Given Admin logged in
    When Go in System Setup
    And Click on UserManagement
    Then Validate Page Title "User management"
    When Click on Add
    And Fill First Name "<First_Name>"
    And Fill Last Name "<Last_Name>"
    And Fill EmailID "<Email>"
    And Flag on in Side Panel of "Work time planning"
    And Close Side Panel
    And Set language from dropdown "English"
    And Set Primary from Dropdown "Dashboard"
    And Click on Save button
    Then Validate Toast Message "User Successfully Added"

    Examples: 
      | First_Name |  | Last_Name  |  | Email               |
      | Pcucumber5 |  | Lcucumber5 |  | Cucu1mber5@gmail.com |

 
  Scenario Outline: ADD NEW USER DONT FILL MANDATORY FIELDS
    Given Admin logged in
    When Go in System Setup
    And Click on UserManagement
    Then Validate Page Title "User management"
    When Click on Add
    And Fill First Name "<First_Name>"
    And Fill Last Name "<Last_Name>"
    And Flag on in Side Panel of "Work time planning"
    And Close Side Panel
    And Fill PhoneNumber "<MobileNumber>"
    And Set language from dropdown "English"
    And Set Primary from Dropdown "Dashboard"
    And Click on Save button
    Then Validate Toast Message "Fill all the required details."

    Examples: 
      | First_Name |  | Last_Name  |
      | Pcucumber1 |  | Lcucumber1 |
